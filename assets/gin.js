const constraints = {
  video: true
};
const video = document.querySelector('video');

navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
  video.srcObject = stream
});

document.addEventListener('gesturestart', function (e) {
  e.preventDefault();
});
document.addEventListener('gesturestart', (e) => {
  e.preventDefault();
});

const ginArray = [
  { name: "Cheap Gin", optic: 0, picture: 'http://logo-load.com/uploads/posts/2016-08/aldi-logo.png' },
  { name: "Pink Gin", optic: 1, picture: 'https://edgmedia.bws.com.au/bws/media/products/673304-1.png?impolicy=Prod_MD' },
  { name: "Pricy Gin", optic: 2, picture: 'https://images.supercall.com/v1/image/2760778/size/tmg-facebook_social' },
  { name: "Alco Free", optic: 3, picture: 'https://images.supercall.com/v1/image/2760778/size/tmg-facebook_social' }
]
const mixerArray = [
  { name: "Tonic", optic: 4, picture: 'https://www.dollargeneral.com/media/catalog/product/cache/image/700x700/e9c3970ab036de70892d86c6d221abfe/1/7/17589701.jpg' },
  { name: "Lemondade", optic: 5, picture: 'https://digitalcontent.api.tesco.com/v1/media/ghs/snapshotimagehandler_1788763864.jpeg?h=540&w=540' }
]
const opticDistances = [10, 10, 10, 10, 10, 10, 10]
const opticTimes = [1, 1, 1, 1, 1, 1, 99];

function generateCSV(ginOptic, mixerOptic) {
  var finalCSV = "";

  for (var i = 0; i < opticDistances.length; i++) {
    var twoChars = (s) => (s.toString().length < 2) ? ("0" + s.toString()) : (s.toString())
    finalCSV = finalCSV + `${twoChars(opticDistances[i])},${twoChars(opticTimes[i])},${twoChars(((ginOptic === i) || (mixerOptic === i)) ? "1" : "0")},`
  }
  return finalCSV;
}

var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    app_state: 'front_page',
    dispensed: {},
    alcoholic: true,
    show_loading: false
  },
  methods: {
    ginMeUp() {
      video.pause();
      app.show_loading = true;
      setTimeout(() => {
        if (app.alcoholic) {
          app.dispensed.selectedGin = ginArray[Math.floor(Math.random() * ginArray.length)]
        } else {
          app.dispensed.selectedGin = ginArray[ginArray.length - 1]
        }
        app.dispensed.selectedMixer = mixerArray[Math.floor(Math.random() * mixerArray.length)]
        var csvString = generateCSV(app.dispensed.selectedGin.optic, app.dispensed.selectedMixer.optic);
        $.ajax({
          type: "POST",
          url: "/makegin",
          dataType: "json",
          contentType: "application/json",
          data: JSON.stringify({ csvString }),
          success: console.log
        })
        app.show_loading = false;
        app.app_state = 'collect_drink'
      }, 2500)
    }
  }
})