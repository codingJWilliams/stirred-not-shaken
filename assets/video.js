const constraints = {
    video: true
};
var alcoholic = true;

$("#alcoFree").click(function () {
    if (alcoholic) {
        $("#alcoFree").text("Switch To Gin Mode");
        $("#alcoFree").css({
            "background-color": "#33cc33",
            "border-color": "#33cc33"
        })
        alcoholic = false;
    } else {
        $("#alcoFree").text("Switch To Alcohol Free");
        $("#alcoFree").css({
            "background-color": "#0096ff",
            "border-color": "#0096ff"
        })
        alcoholic = true;
    }
    console.log(alcoholic)
})

const ginArray = [
    {name: "Cheap Gin", optic: 0, picture: 'http://logo-load.com/uploads/posts/2016-08/aldi-logo.png'},
    {name: "Pink Gin", optic: 1, picture: 'https://edgmedia.bws.com.au/bws/media/products/673304-1.png?impolicy=Prod_MD'},
    {name: "Pricy Gin", optic: 2, picture: 'https://images.supercall.com/v1/image/2760778/size/tmg-facebook_social'},
    {name: "Alco Free", optic: 3, picture: 'https://images.supercall.com/v1/image/2760778/size/tmg-facebook_social'}
]
const mixerArray = [
    {name: "Tonic", optic: 4, picture: 'https://www.dollargeneral.com/media/catalog/product/cache/image/700x700/e9c3970ab036de70892d86c6d221abfe/1/7/17589701.jpg'},
    {name: "Lemondade", optic: 5, picture: 'https://digitalcontent.api.tesco.com/v1/media/ghs/snapshotimagehandler_1788763864.jpeg?h=540&w=540'}
]
const opticDistances = [10, 10, 10, 10, 10, 10, 10]
const opticTimes = [1, 1, 1, 1, 1, 1, 99];

function generateCSV(ginOptic, mixerOptic) {
    var finalCSV = "";

    for (var i = 0; i < opticDistances.length; i++) {
        var twoChars = (s) => (s.toString().length < 2) ? ("0"+s.toString()) : (s.toString())
        finalCSV = finalCSV + `${twoChars(opticDistances[i])},${twoChars(opticTimes[i])},${twoChars(((ginOptic === i) || (mixerOptic === i)) ? "1" : "0")},`
    }
    return finalCSV;
}

const video = document.querySelector('video');

navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
    video.srcObject = stream
});

document.addEventListener('gesturestart', function (e) {
    e.preventDefault();
});
document.addEventListener('gesturestart', (e) => {
    e.preventDefault();
});

$("#ginMeUp").click(function () {
    video.pause();
    $("#video_overlays2").hide()
    $("#video_overlays").show();
    setTimeout(() => {
        if (alcoholic) {
            var selectedGin = ginArray[Math.floor(Math.random() * ginArray.length)]
        } else {
            var selectedGin = ginArray[ginArray.length - 1]
        }
        var selectedMixer = mixerArray[Math.floor(Math.random() * mixerArray.length)]
        $("#firstImg").attr("src", selectedGin.picture)
        $("#secondImg").attr("src", selectedMixer.picture)
        $("#drinkerCallout").text(`You look like a ${selectedGin.name} & ${selectedMixer.name} drinker!`)
        var csvString = generateCSV(selectedGin.optic, selectedMixer.optic);
        $.ajax({
            type: "POST",
            url: "/makegin",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({csvString}),
            success: console.log
        })
        $(".shouldBeSwapped").hide();
        $(".hasBeenSwapped").show();
    }, 2500)
})

$("#backToHome").click(function () {
    video.play()
    $("#video_overlays2").show()
    $("#video_overlays").hide();
    $(".shouldBeSwapped").show();
    $(".hasBeenSwapped").hide();
})