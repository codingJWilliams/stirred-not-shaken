const SerialPort = require('serialport')
const port = new SerialPort('COM3', {
  baudRate: 115200
})
const express = require('express')
const app = express();
const bodyParser = require("body-parser");

app.set("view engine", "ejs")

app.use(express.static("assets"));
app.use(bodyParser.json())


app.get('/', (req, res) => {
    res.render('pages/gin');
    //port.write("a");
})
app.get('/juice', (req, res) => {
    res.render('pages/juice');
    //port.write("a");
})

app.post("/makegin", (req, res) => {
    console.log("Dispensing drink - " + req.body.csvString)
    
    res.end("ok")
    setTimeout(() => { 
        port.write(req.body.csvString);
    }, 3000);
})

app.listen(3000, () => console.log(`Example app listening on port 3000!`))